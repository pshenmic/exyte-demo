require("dotenv").config()

const logger = require("./src/utils/logger")
const app = require("./src/app")

app
    .start()
    .then(() => logger.info("Exyte demo has been started"))
    .catch((e) => logger.error(e))

