FROM node:12
RUN mkdir -p /app/src
RUN mkdir -p /app/public
RUN mkdir -p /app/solidity
WORKDIR /app
COPY package-lock.json /app
COPY package.json /app
RUN npm install
COPY src /app/src
COPY public /app/public
COPY solidity /app/solidity
COPY .env /app
COPY index.js /app
CMD ["node", "/app/index.js"]
