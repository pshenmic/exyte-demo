const logger = require("../utils/logger")
const ErrorTypes = require("../enums/error.types")

module.exports = (error, request, reply) => {
    if(error.message === ErrorTypes.VALIDATION_ERROR) {
        return reply.type("application/json").code(400).send({error: ErrorTypes.VALIDATION_ERROR})
    }

    logger.error(`RequestBody: ${JSON.stringify(request.body)}`)
    logger.error(error)

    reply.type("application/json").code(500).send({error: ErrorTypes.INTERNAL_SERVER_ERROR})
}
