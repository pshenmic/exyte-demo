const MainController = require("../controllers/main.controller")
const CalculatorService = require("../service/calculator.service")
const Web3Service = require("../service/web3.service")

const errorHandler = require("./error.handler")

function Routes({fastify, web3}) {

    const web3Service = new Web3Service({web3})
    const calculatorService = new CalculatorService({web3Service})

    const mainController = new MainController({calculatorService, web3Service})

    fastify.post("/api/v1/calculator/plus", mainController.plus)
    fastify.post("/api/v1/calculator/minus", mainController.minus)
    fastify.post("/api/v1/calculator/multiply", mainController.multiply)
    fastify.post("/api/v1/calculator/divide", mainController.divide)

    fastify.get("/api/v1/calculator/history", mainController.history)

    fastify.setErrorHandler(errorHandler)
}

module.exports = Routes
