class CalculationResultDTO {

    constructor({val1, val2, result, operation}) {
        this.val1 = val1.toNumber()
        this.val2 = val2.toNumber()
        this.result = result.toNumber()
        this.operation = operation
    }

}

module.exports = CalculationResultDTO
