const Operation = require("../enums/operation")

class CalculationResult {

    constructor({val1, val2, result, operationIndex}) {
        this.val1 = val1
        this.val2 = val2
        this.result = result
        this.operation = Object.keys(Operation)[operationIndex]
    }

}

module.exports = CalculationResult
