const CalculationResultDTO = require("../models/calculation.result.dto")
const Operation = require("../enums/operation")
const ErrorTypes = require("../enums/error.types")
const ValidationUtils = require("../utils/validation.utils")

class MainController {

    constructor({calculatorService}) {
        this.calculatorService = calculatorService

        this.history = this.history.bind(this)
        this.plus = this.plus.bind(this)
        this.minus = this.minus.bind(this)
        this.multiply = this.multiply.bind(this)
        this.divide = this.divide.bind(this)
    }

    async history(request, reply) {
        const calculationHistory = await this.calculatorService.getHistory()

        return reply.type("application/json").code(200).send(calculationHistory.map(calculationResult => (new CalculationResultDTO(calculationResult))))
    }

    async plus(request, reply) {
        const val1 = request.body && request.body.val1
        const val2 = request.body && request.body.val2

        if (!ValidationUtils.validateNumber(val1) || !ValidationUtils.validateNumber(val2)) {
            throw new Error(ErrorTypes.VALIDATION_ERROR)
        }

        const calculationResult = await  this.calculatorService.compute(val1, val2, Operation.PLUS)

        return reply.type("application/json").code(200).send(new CalculationResultDTO(calculationResult))
    }


    async minus(request, reply) {
        const val1 = request.body && request.body.val1
        const val2 = request.body && request.body.val2

        if (!ValidationUtils.validateNumber(val1) || !ValidationUtils.validateNumber(val2)) {
            throw new Error(ErrorTypes.VALIDATION_ERROR)
        }

        const calculationResult = await  this.calculatorService.compute(val1, val2, Operation.MINUS)

        return reply.type("application/json").code(200).send(new CalculationResultDTO(calculationResult))
    }


    async multiply(request, reply) {
        const val1 = request.body && request.body.val1
        const val2 = request.body && request.body.val2

        if (!ValidationUtils.validateNumber(val1) || !ValidationUtils.validateNumber(val2)) {
            throw new Error(ErrorTypes.VALIDATION_ERROR)
        }

        const calculationResult = await  this.calculatorService.compute(val1, val2, Operation.MULTIPLY)

        return reply.type("application/json").code(200).send(new CalculationResultDTO(calculationResult))
    }


    async divide(request, reply) {
        const val1 = request.body && request.body.val1
        const val2 = request.body && request.body.val2

        if (!ValidationUtils.validateNumber(val1) || !ValidationUtils.validateNumber(val2)) {
            throw new Error(ErrorTypes.VALIDATION_ERROR)
        }

        const calculationResult = await  this.calculatorService.compute(val1, val2, Operation.DIVIDE)

        return reply.type("application/json").code(200).send(new CalculationResultDTO(calculationResult))
    }

}

module.exports = MainController
