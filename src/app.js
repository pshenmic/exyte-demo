const path = require("path")
const logger = require("./utils/logger")
const fastify = require("fastify")({})
const fastifyStatic = require("fastify-static")
const Web3 = require("web3")
const Routes = require("./routes")
const fastifyServerTimeout = require("fastify-server-timeout")

const start = async () => {

    if (!process.env.CONTRACT_ADDRESS) {
        throw new Error("Contract address not defined (CONTRACT_ADDRESS env not set)")
    }

    if (!process.env.OWNER_ADDRESS) {
        throw new Error("Contract address not defined (OWNER_ADDRESS env not set)")
    }

    if (!process.env.OWNER_PRIVATE_KEY) {
        throw new Error("Contract address not defined (OWNER_PRIVATE_KEY env not set)")
    }

    if (!process.env.INFURA_PROJECT_ID) {
        throw new Error("Contract address not defined (INFURA_PROJECT_ID env not set)")
    }

    const web3 = new Web3(new Web3.providers.WebsocketProvider(`wss://ropsten.infura.io/ws/v3/${process.env.INFURA_PROJECT_ID}`))


    fastify.register(fastifyServerTimeout, {
        serverTimeout: Number(process.env.CALCULATION_TIMEOUT) * 1000
    })

    fastify.register(fastifyStatic, {
        root: path.join(__dirname, "..", "public")
    })

    Routes({fastify, web3})

    // 0.0.0.0 is required for deploying in docker
    fastify.listen(process.env.BACKEND_PORT, "0.0.0.0", (err) => {
        if (err) throw err
        logger.info(`Server started on port ${process.env.BACKEND_PORT}`)
    })
}

const stop = async () => {
    logger.info("Stopping Exyte demo webserver")
    fastify.close()
}

module.exports = {
    start,
    stop
}
