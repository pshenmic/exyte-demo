const ErrorTypes = require("../enums/error.types")
const Operation = require("../enums/operation")
const CalculationResult = require("../models/calculation.result")
const logger = require("../utils/logger")

class CalculatorService {

    constructor({web3Service}) {
        this.web3Service = web3Service
    }

    // Computing
    // 1) Send transaction to the contract compute function with appropriate params
    // 2) Wait for confirmation (result)
    // 3) Return CalculationResult
    async compute(val1, val2, operation) {
        let methodName
        switch (operation) {
            case Operation.PLUS:
                methodName = "plus"
                break
            case Operation.MINUS:
                methodName = "minus"
                break
            case Operation.MULTIPLY:
                methodName = "multiply"
                break
            case Operation.DIVIDE:
                methodName = "divide"
        }

        const txHash = await this.web3Service.callCalculatorMethod(methodName, [val1, val2])
        const transactionReceipt = await this.web3Service.awaitTransactionReceipt(txHash)
        const decoded = await this.web3Service.decodeCalculationResult(transactionReceipt)

        return new CalculationResult({
            val1: decoded[0],
            val2: decoded[1],
            result: decoded[2],
            operationIndex: decoded[3]
        })
    }

    // History
    // 1) Get all pastEvents from contract
    // 2) map to CalculationResult[]
    // 3) Return CalculationResult[]
    async getHistory() {
        const pastEvents = await this.web3Service.getPastCalculationResultEvents()

        return pastEvents.map(event => (new CalculationResult({
            val1: event.returnValues[0],
            val2: event.returnValues[1],
            result: event.returnValues[2],
            operationIndex: event.returnValues[3]
        })))
    }


}

module.exports = CalculatorService
