const Tx = require("ethereumjs-tx")
const CalculatorAbi = require("../../solidity/Calculator.abi")
const ErrorTypes = require("../enums/error.types")
const logger = require("../utils/logger")

// Computing
// 1) Send transaction to the contract compute function with appropriate params
// 2) Wait for confirmation (result)
// 3) Return CalculationResult

// History
// 1) Get all pastEvents from contract
// 2) map to CalculationResult[]
// 3) Return CalculationResult[]

class Web3Service {

    constructor({web3}) {
        this.web3 = web3
        this.Calculator = new web3.eth.Contract(CalculatorAbi, process.env.CONTRACT_ADDRESS)

        this.callCalculatorMethod = this.callCalculatorMethod.bind(this)
        this.decodeCalculationResult = this.decodeCalculationResult.bind(this)
        this.awaitTransactionReceipt = this.awaitTransactionReceipt.bind(this)
        this.getPastCalculationResultEvents = this.getPastCalculationResultEvents.bind(this)
    }

    async callCalculatorMethod(methodName, params) {
        return new Promise(async (resolve, reject) => {
            const call = await this.Calculator.methods[methodName](...params)
            const data = call.encodeABI()

            const transactionCount = await this.web3.eth.getTransactionCount(process.env.OWNER_ADDRESS)

            const rawTx = {
                nonce: "0x" + (transactionCount).toString(16),
                gasPrice: ("0x" + (Number(process.env.GAS_PRICE_GWEI) * 1e9).toString(16)),
                gasLimit: ("0x" + (Number(process.env.GAS_LIMIT)).toString(16)),
                to: process.env.CONTRACT_ADDRESS,
                value: "0x00",
                data
            }

            const tx = new Tx(rawTx)
            tx.sign(Buffer.from(process.env.OWNER_PRIVATE_KEY.substr(2), "hex"))

            const serializedTx = tx.serialize()

            this.web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"), (error, txHash) => {
                if (error) {
                    logger.error(error)
                    reject("WEB3_BROADCAST_ERROR")
                }
                resolve(txHash)
            })
        })
    }

    async decodeCalculationResult(transactionReceipt) {
        const {logs} = transactionReceipt
        const [resultLog] = logs

        return this.web3.eth.abi.decodeParameters(["int256", "int256", "int256", "uint8"], resultLog.data)
    }

    async awaitTransactionReceipt(txHash) {
        return new Promise((resolve, reject) => {

            const pollTransactionReceipt = () => {
                this.web3.eth.getTransactionReceipt(txHash)
                    .then((transactionReceipt) => {
                        if (!transactionReceipt) {
                            setTimeout(pollTransactionReceipt, Number(process.env.TRANSACTION_RECEIPT_POLL_DELAY) * 1000)
                        } else {
                            resolve(transactionReceipt)
                        }
                    })
                    .catch(reject)
            }

            setTimeout(() => reject(ErrorTypes.CALCULATION_TIMEOUT), Number(process.env.CALCULATION_TIMEOUT) * 1000)

            pollTransactionReceipt()
        })

    }

    async getPastCalculationResultEvents() {
        return await this.Calculator.getPastEvents("allEvents", {
            fromBlock: 5691581,
            toBlock: "latest"
        })
    }


}

module.exports = Web3Service
