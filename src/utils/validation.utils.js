class ValidationUtils {

    static validateNumber(number) {
        return Number.isInteger(number)
    }

}

module.exports = ValidationUtils
