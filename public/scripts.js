async function compute(operation) {

    const val1 = Number(document.getElementById("val1").value)
    const val2 = Number(document.getElementById("val2").value)

    drawCalculationResult("computing...")

    try {
        const response = await window.fetch("/api/v1/calculator/" + operation.toLowerCase(), {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                val1,
                val2
            })
        })

        if (response.status !== 200) {
            throw new Error()
        }

        const json = await response.json()

        drawCalculationResult(renderComputationResult(json.val1, json.val2, json.operation, json.result))

        loadHistory()
    } catch (e) {
        drawCalculationResult("error, try again later")
    }
}


function loadHistory() {
    const loadData = async () => {
        const response = await window.fetch("/api/v1/calculator/history")

        if (response.status !== 200) {
            throw new Error()
        }

        return await response.json()
    }

    loadData()
        .then((json) => drawHistory(json))
        .catch(() => drawHistoryLoadError())
}

function drawCalculationResult(string) {
    document.getElementById("calculator_result").innerHTML = "Result: " + string
}


function clearHistory() {
    const myNode = document.getElementById("history_container")
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild)
    }
}

function drawHistory(json) {
    clearHistory()
    json.forEach((calculationResult) => {
        const {val1, val2, result, operation} = calculationResult

        const div = document.createElement("div")
        div.innerHTML = renderComputationResult(val1, val2, operation, result)

        document.getElementById("history_container").appendChild(div)
    })
}

function renderComputationResult(val1, val2, operation, result) {
    return `${val1} ${mapOperationToSymbol(operation)} ${val2} = ${result}`
}

function drawHistoryLoadError() {
    clearHistory()

    const div = document.createElement("div")
    div.innerHTML = `Error while loading history`

    document.getElementById("history_container").appendChild(div)
}

function mapOperationToSymbol(operation) {
    switch (operation) {
        case "PLUS":
            return "+"
        case "MINUS":
            return "-"
        case "DIVIDE":
            return "/"
        case "MULTIPLY":
            return "*"
    }
}


loadHistory()
