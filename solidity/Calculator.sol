pragma solidity ^0.5.1;

contract Calculator {

    enum Operation { PLUS, MINUS, MULTIPLY, DIVISION }

    event CalculationResult(int256 val1, int256 val2, int256 result, uint8 operation);

    function plus(int256 val1, int256 val2) public returns (int256) {
        int256 result = val1 + val2;

        emit CalculationResult(val1, val2, result, 0);

        return result;
    }

    function minus(int256 val1, int256 val2) public returns (int256) {
        int256 result = val1 - val2;

        emit CalculationResult(val1, val2, result, 1);

        return result;
    }

    function multiply(int256 val1, int256 val2) public returns (int256) {
        int256 result = val1 * val2;

        emit CalculationResult(val1, val2, result, 2);

        return result;
    }

    function divide(int256 val1, int256 val2) public returns (int256) {
        int256 result = val1 / val2;

        emit CalculationResult(val1, val2, result, 3);

        return result;
    }

}
