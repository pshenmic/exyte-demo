module.exports = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "val1",
                "type": "int256"
            },
            {
                "name": "val2",
                "type": "int256"
            }
        ],
        "name": "plus",
        "outputs": [
            {
                "name": "result",
                "type": "int256"
            }
        ],
        "payable": false,
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "val1",
                "type": "int256"
            },
            {
                "name": "val2",
                "type": "int256"
            }
        ],
        "name": "minus",
        "outputs": [
            {
                "name": "result",
                "type": "int256"
            }
        ],
        "payable": false,
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "val1",
                "type": "int256"
            },
            {
                "name": "val2",
                "type": "int256"
            }
        ],
        "name": "multiply",
        "outputs": [
            {
                "name": "result",
                "type": "int256"
            }
        ],
        "payable": false,
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "val1",
                "type": "int256"
            },
            {
                "name": "val2",
                "type": "int256"
            }
        ],
        "name": "divide",
        "outputs": [
            {
                "name": "result",
                "type": "int256"
            }
        ],
        "payable": false,
        "type": "function"
    },
    {
        "name": "CalculationResult",
        "type": "event",
        "inputs": [
            {
                "name": "val1",
                "type": "int256"
            },
            {
                "name": "val2",
                "type": "int256"
            },
            {
                "name": "result",
                "type": "int256"
            },
            {
                "name": "operation",
                "type": "uint8"
            }
        ]
    }
]
